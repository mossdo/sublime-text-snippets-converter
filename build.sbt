name := "sublime-snippets-converter"

version := "1.0"

libraryDependencies += "org.rogach" %% "scallop" % "0.9.5"

libraryDependencies += "com.github.scala-incubator.io" %% "scala-io-core" % "0.4.3"

libraryDependencies += "com.github.scala-incubator.io" %% "scala-io-file" % "0.4.3"

    
package io.moss.sublimeconverter

object Main {

  def main(args: Array[String]) {
    Converter(new Args(args))
  }

}

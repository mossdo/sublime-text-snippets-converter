package io.moss.sublimeconverter

import scalax.file.{PathSet, Path}
import scala.xml.{Node, XML}

object Converter {

  def apply(args: Args) = {

    implicit val codec = scalax.io.Codec.UTF8
    val snippetDir = Path.fromString(args.snippetDir())
    val outputDir = Path.fromString(args.outputDir())
    val outputFile = outputDir / "%s.xml".format(args.liveTemplateGroupName())

    if (snippetDir.nonExistent) {
      println("The snippet directory %s does not exist.".format(snippetDir))
    }

    if (outputDir.nonExistent) {
      println("The output directory %s does not exist.".format(outputDir))
    }

    println("Writing to output file %s".format(outputFile))

    val ideaStuff = ideaTemplate(convert(snippetDir), args.liveTemplateGroupName()).toString
    outputFile.write(ideaStuff)

  }

  def pathToSnippet(path: Path): SublimeSnippet = {
    SublimeSnippet(XML.loadFile(path.path))
  }


  def convert(snippetDir: Path): PathSet[SublimeSnippet] = {
    (snippetDir ** "*.sublime-snippet").map(path => pathToSnippet(path)).asInstanceOf[PathSet[SublimeSnippet]]
  }


  val ideaTemplate = { (sublimeSnippets: PathSet[SublimeSnippet], group: String) =>
    <templateSet group={group}>
      {for (snippet <- sublimeSnippets) yield
      <template name={snippet.trigger} value={snippet.content} toReformat="true" toShortenFQNames="true">
        <context>
          <option name="HTML_TEXT" value="true"/>
          <option name="HTML" value="true"/>
          <option name="XSL_TEXT" value="false"/>
          <option name="XML" value="false"/>
          <option name="JAVA_CODE" value="false"/>
          <option name="JAVA_STATEMENT" value="false"/>
          <option name="JAVA_EXPRESSION" value="false"/>
          <option name="JAVA_DECLARATION" value="false"/>
          <option name="JAVA_COMMENT" value="false"/>
          <option name="JAVA_STRING" value="false"/>
          <option name="COMPLETION" value="false"/>
          <option name="CSS_PROPERTY_VALUE" value="false"/>
          <option name="CSS_DECLARATION_BLOCK" value="false"/>
          <option name="CSS_RULESET_LIST" value="false"/>
          <option name="CSS" value="false"/>
          <option name="JSP" value="false"/>
          <option name="JAVA_SCRIPT" value="false"/>
          <option name="SQL" value="false"/>
          <option name="CUCUMBER_FEATURE_FILE" value="false"/>
          <option name="MAVEN" value="false"/>
          <option name="ASPECTJ" value="false"/>
          <option name="SCALA" value="false"/>
          <option name="HAML" value="false"/>
          <option name="OTHER" value="false"/>
        </context>
      </template>}
    </templateSet>
  }

}

case class SublimeSnippet(trigger: String, description: String, scope: String, content: String) {
}

object SublimeSnippet {

  def apply(node: Node): SublimeSnippet = {
    SublimeSnippet(
      (node \\ "tabTrigger").text,
      (node \\ "description").text,
      (node \\ "scope").text,
      (node \\ "content").text
    )
  }

}

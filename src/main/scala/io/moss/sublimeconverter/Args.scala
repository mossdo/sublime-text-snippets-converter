package io.moss.sublimeconverter

import org.rogach.scallop.ScallopConf
import org.rogach.scallop.exceptions.ScallopException

class Args(args: Seq[String] = Nil) extends ScallopConf(args) {

  guessOptionName = true

  version("Sublime Snippet Converter v1")

  banner(
    """
      |Sublime Snippet Converter is a simple utility for converting sublime snippets into
      | live-templates for IntelliJ IDEA v12+
      |
    """.stripMargin)

  val snippetDir = opt[String]("snippet-dir", descr="Path to a directory containing SublimeText snippets.", required = true)
  val outputDir = opt[String]("output-dir", descr="Path to a directory where the live-template definition should be created.", default = Option("."))
  val liveTemplateGroupName = opt[String]("name", descr="Name of the live-template group to output.", required = true)


  override def onError(e: Throwable) = e match {
    case ScallopException(message) =>
      println(message)
      printHelp
      System.exit(1)
    case ex => super.onError(ex)
  }

}
